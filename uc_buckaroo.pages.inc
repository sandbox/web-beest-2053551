<?php
function uc_buckaroo_complete() {
  $order_id = filter_xss( $_POST['brq_invoicenumber'] );
  $order = uc_order_load( $order_id );
  
  if( $order->order_status == 'payment_received' ) {
    // payment already processed through push.
    drupal_set_message( t('Thank you for your order.') );  
    drupal_goto('cart/checkout/complete');
  }
}

function uc_buckaroo_notification($cart_id = 0) {
  watchdog('uc_buckaroo', 'UC BUCKAROO PUSH NOTIFICATION: ' . serialize( $_POST ));
  
  $brq_signature = filter_xss($_POST['brq_signature']);
  $order = uc_order_load(filter_xss($_POST['brq_invoicenumber']));
  
  unset($_POST['brq_signature']);
  
  $check_signature = _uc_buckaroo_create_secret($_POST);
  $process_payment = FALSE;
  
  // signatures match. message is valid.
  if($check_signature == $brq_signature) {
    $process_payment = _uc_buckaroo_process_status_code($_POST);    
    
    // login in order
    uc_order_comment_save($order->order_id, 0, t('Message received from Buckaroo.'), 'admin');

    if( !$process_payment ) {    
      // Check merchant account
      if (variable_get('buckaroo_merchant_id', '') != filter_xss($_POST['brq_websitekey'] ) ) {
        $errors[] = t('Incorrect merchant account: @merchant.', array('@merchant' => $_POST['brq_websitekey'] ) );
      }
  
      // Check status
      if ($order->order_status != 'in_checkout' &&
        $order->order_status != 'pending') {
        $errors[] = t('Order was neither in checkout nor pending.');
      }
  
      // Check price
      $amount = filter_xss($_POST['brq_amount']);
      if (round($order->order_total, 2) != $amount ) {
        $errors[] = t('Incorrect price: @price1 vs @price2.', array('@price1' => round($order->order_total, 2), '@price2' => $amount));
      }
  
      // Check currency
      $currency = filter_xss($_POST['brq_currency']);
      if (variable_get('uc_currency_code', 'EUR') != $currency) {
        $errors[] = t('Incorrect currency code @currency.', array('@currency' => $currency));
      }
    }
  }
  else {
    $errors[] = t('Signatures do not match. Given: '.$brq_signature.', calculated: '.$check_signature);
  }
  
  if (!empty($errors)) {
    // Log errors to order and watchdog.
    if ($order) {
      $error_message = 'Order ' . $order->order_id . ' caused error: ' . implode('<br />', $errors);
      uc_order_comment_save($order->order_id, 0, $error_message, 'admin');
    }
    
    $error_data = array(
      '@id' => $order->order_id,
      '@error' => implode('<br />', $errors),
    );
    watchdog('uc_buckaroo', 'Order @id caused error: @error', $error_data, WATCHDOG_ERROR);

    exit();
  }
  
  

  // Data ok, process now
  if( $process_payment ) {
    watchdog('uc_buckaroo', 'Process payment TRUE');

    // Empty cart - can be in test or live system
    if( $cart_id == 0) {
      $cart_id = uc_cart_get_id(false);
    }
    watchdog('uc_buckaroo', 'Cart ID: '.$cart_id);
      
    uc_cart_empty($cart_id);
    
    uc_cart_complete_sale($order, FALSE);
  }
  else {
    uc_order_comment_save($order->order_id, 0, t('Unsuccessful authorisation attempt.'), 'admin');
  }
  return;
}